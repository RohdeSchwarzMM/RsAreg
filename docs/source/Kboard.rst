Kboard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: KBOard:LAYout

.. code-block:: python

	KBOard:LAYout



.. autoclass:: RsAreg.Implementations.Kboard.KboardCls
	:members:
	:undoc-members:
	:noindex: