Range
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:RANGe

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:RANGe



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.Range.RangeCls
	:members:
	:undoc-members:
	:noindex: