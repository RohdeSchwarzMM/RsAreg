Tselected
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:PCIFpga:UPDate:TSELected:CATalog
	single: SYSTem:PCIFpga:UPDate:TSELected:STEP

.. code-block:: python

	SYSTem:PCIFpga:UPDate:TSELected:CATalog
	SYSTem:PCIFpga:UPDate:TSELected:STEP



.. autoclass:: RsAreg.Implementations.System.PciFpga.Update.Tselected.TselectedCls
	:members:
	:undoc-members:
	:noindex: