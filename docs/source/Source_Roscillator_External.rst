External
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce]:ROSCillator:EXTernal:FREQuency
	single: [SOURce]:ROSCillator:EXTernal:SBANdwidth

.. code-block:: python

	[SOURce]:ROSCillator:EXTernal:FREQuency
	[SOURce]:ROSCillator:EXTernal:SBANdwidth



.. autoclass:: RsAreg.Implementations.Source.Roscillator.External.ExternalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.roscillator.external.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Roscillator_External_RfOff.rst