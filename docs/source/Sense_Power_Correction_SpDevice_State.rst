State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:CORRection:SPDevice:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:CORRection:SPDevice:STATe



.. autoclass:: RsAreg.Implementations.Sense.Power.Correction.SpDevice.State.StateCls
	:members:
	:undoc-members:
	:noindex: