Remove
----------------------------------------





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Remove.RemoveCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.osetup.multiInstrument.remove.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Osetup_MultiInstrument_Remove_Execute.rst