State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:HTTP:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:HTTP:[STATe]



.. autoclass:: RsAreg.Implementations.System.Security.Network.Http.State.StateCls
	:members:
	:undoc-members:
	:noindex: