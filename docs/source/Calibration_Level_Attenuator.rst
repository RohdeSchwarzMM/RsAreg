Attenuator
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration<HW>:LEVel:ATTenuator:STAGe

.. code-block:: python

	CALibration<HW>:LEVel:ATTenuator:STAGe



.. autoclass:: RsAreg.Implementations.Calibration.Level.Attenuator.AttenuatorCls
	:members:
	:undoc-members:
	:noindex: