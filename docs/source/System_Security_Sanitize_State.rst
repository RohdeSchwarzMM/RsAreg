State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:SANitize:[STATe]

.. code-block:: python

	SYSTem:SECurity:SANitize:[STATe]



.. autoclass:: RsAreg.Implementations.System.Security.Sanitize.State.StateCls
	:members:
	:undoc-members:
	:noindex: