TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:TYPE

.. code-block:: python

	SENSe<CH>:[POWer]:TYPE



.. autoclass:: RsAreg.Implementations.Sense.Power.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: