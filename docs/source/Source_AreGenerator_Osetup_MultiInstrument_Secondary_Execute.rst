Execute
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:EXECute

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:EXECute



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.Execute.ExecuteCls
	:members:
	:undoc-members:
	:noindex: