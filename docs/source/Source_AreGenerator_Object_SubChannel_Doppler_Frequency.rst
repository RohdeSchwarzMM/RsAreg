Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:DOPPler:FREQuency

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:DOPPler:FREQuency



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.SubChannel.Doppler.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: