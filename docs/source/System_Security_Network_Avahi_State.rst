State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:AVAHi:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:AVAHi:[STATe]



.. autoclass:: RsAreg.Implementations.System.Security.Network.Avahi.State.StateCls
	:members:
	:undoc-members:
	:noindex: