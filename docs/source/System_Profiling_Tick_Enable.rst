Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PROFiling:TICK:ENABle

.. code-block:: python

	SYSTem:PROFiling:TICK:ENABle



.. autoclass:: RsAreg.Implementations.System.Profiling.Tick.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: