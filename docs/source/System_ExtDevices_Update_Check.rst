Check
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:EXTDevices:UPDate:CHECk

.. code-block:: python

	SYSTem:EXTDevices:UPDate:CHECk



.. autoclass:: RsAreg.Implementations.System.ExtDevices.Update.Check.CheckCls
	:members:
	:undoc-members:
	:noindex: