Measure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:SPC:MEASure

.. code-block:: python

	[SOURce<HW>]:POWer:SPC:MEASure



.. autoclass:: RsAreg.Implementations.Source.Power.Spc.Measure.MeasureCls
	:members:
	:undoc-members:
	:noindex: