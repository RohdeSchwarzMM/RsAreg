Doppler
----------------------------------------





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.SubChannel.Doppler.DopplerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.object.subChannel.doppler.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Object_SubChannel_Doppler_Frequency.rst
	Source_AreGenerator_Object_SubChannel_Doppler_Speed.rst