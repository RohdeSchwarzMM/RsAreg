Ulock
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:ULOCk

.. code-block:: python

	SYSTem:ULOCk



.. autoclass:: RsAreg.Implementations.System.Ulock.UlockCls
	:members:
	:undoc-members:
	:noindex: