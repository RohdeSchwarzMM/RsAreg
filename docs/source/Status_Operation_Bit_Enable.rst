Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:OPERation:BIT<BITNR>:ENABle

.. code-block:: python

	STATus:OPERation:BIT<BITNR>:ENABle



.. autoclass:: RsAreg.Implementations.Status.Operation.Bit.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: