Check
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PCIFpga:UPDate:CHECk

.. code-block:: python

	SYSTem:PCIFpga:UPDate:CHECk



.. autoclass:: RsAreg.Implementations.System.PciFpga.Update.Check.CheckCls
	:members:
	:undoc-members:
	:noindex: