Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration<HW>:LEVel:STATe

.. code-block:: python

	CALibration<HW>:LEVel:STATe



.. autoclass:: RsAreg.Implementations.Calibration.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.level.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_Level_Attenuator.rst