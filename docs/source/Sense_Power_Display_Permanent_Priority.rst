Priority
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:DISPlay:PERManent:PRIority

.. code-block:: python

	SENSe<CH>:[POWer]:DISPlay:PERManent:PRIority



.. autoclass:: RsAreg.Implementations.Sense.Power.Display.Permanent.Priority.PriorityCls
	:members:
	:undoc-members:
	:noindex: