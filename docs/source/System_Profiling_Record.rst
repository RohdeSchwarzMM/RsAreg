Record
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:PROFiling:RECord
	single: SYSTem:PROFiling:RECord:CLEar
	single: SYSTem:PROFiling:RECord:IGNore
	single: SYSTem:PROFiling:RECord:SAVE

.. code-block:: python

	SYSTem:PROFiling:RECord
	SYSTem:PROFiling:RECord:CLEar
	SYSTem:PROFiling:RECord:IGNore
	SYSTem:PROFiling:RECord:SAVE



.. autoclass:: RsAreg.Implementations.System.Profiling.Record.RecordCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.profiling.record.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Profiling_Record_Count.rst
	System_Profiling_Record_Wrap.rst