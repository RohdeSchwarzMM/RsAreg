Add
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:ADD

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary:ADD



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.Add.AddCls
	:members:
	:undoc-members:
	:noindex: