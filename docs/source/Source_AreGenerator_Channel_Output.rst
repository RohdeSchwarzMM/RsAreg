Output
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:OUTPut:NOMGain

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:OUTPut:NOMGain



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Channel.Output.OutputCls
	:members:
	:undoc-members:
	:noindex: