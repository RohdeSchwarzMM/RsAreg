Needed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:EXTDevices:UPDate:NEEDed:[STATe]

.. code-block:: python

	SYSTem:EXTDevices:UPDate:NEEDed:[STATe]



.. autoclass:: RsAreg.Implementations.System.ExtDevices.Update.Needed.NeededCls
	:members:
	:undoc-members:
	:noindex: