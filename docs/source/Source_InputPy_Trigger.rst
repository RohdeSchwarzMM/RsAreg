Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:INPut:TRIGger:SLOPe

.. code-block:: python

	[SOURce]:INPut:TRIGger:SLOPe



.. autoclass:: RsAreg.Implementations.Source.InputPy.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: