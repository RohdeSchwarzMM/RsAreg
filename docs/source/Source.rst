Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce<HW>:PRESet

.. code-block:: python

	SOURce<HW>:PRESet



.. autoclass:: RsAreg.Implementations.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator.rst
	Source_Bb.rst
	Source_Frequency.rst
	Source_InputPy.rst
	Source_Modulation.rst
	Source_Path.rst
	Source_Power.rst
	Source_Roscillator.rst