State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MMEMory:STORe:STATe

.. code-block:: python

	MMEMory:STORe:STATe



.. autoclass:: RsAreg.Implementations.MassMemory.Store.State.StateCls
	:members:
	:undoc-members:
	:noindex: