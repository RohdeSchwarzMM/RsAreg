Condition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<BITNR>:CONDition

.. code-block:: python

	STATus:QUEStionable:BIT<BITNR>:CONDition



.. autoclass:: RsAreg.Implementations.Status.Questionable.Bit.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: