Device
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:STATus:[DEVice]

.. code-block:: python

	SENSe<CH>:[POWer]:STATus:[DEVice]



.. autoclass:: RsAreg.Implementations.Sense.Power.Status.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: