User
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:FILTer:LENGth:[USER]

.. code-block:: python

	SENSe<CH>:[POWer]:FILTer:LENGth:[USER]



.. autoclass:: RsAreg.Implementations.Sense.Power.FilterPy.Length.User.UserCls
	:members:
	:undoc-members:
	:noindex: