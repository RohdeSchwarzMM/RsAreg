Set
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:SERRor:SET

.. code-block:: python

	TEST:SERRor:SET



.. autoclass:: RsAreg.Implementations.Test.Serror.Set.SetCls
	:members:
	:undoc-members:
	:noindex: