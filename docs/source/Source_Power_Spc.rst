Spc
----------------------------------------





.. autoclass:: RsAreg.Implementations.Source.Power.Spc.SpcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.power.spc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Power_Spc_Measure.rst
	Source_Power_Spc_Single.rst