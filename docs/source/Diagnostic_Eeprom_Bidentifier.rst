Bidentifier
----------------------------------------





.. autoclass:: RsAreg.Implementations.Diagnostic.Eeprom.Bidentifier.BidentifierCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.diagnostic.eeprom.bidentifier.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Diagnostic_Eeprom_Bidentifier_Catalog.rst