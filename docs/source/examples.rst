Examples
======================

For more examples, visit our `Rohde & Schwarz Github repository <https://github.com/Rohde-Schwarz/Examples/>`_.



.. literalinclude:: RsAreg_GettingStarted_Example.py

