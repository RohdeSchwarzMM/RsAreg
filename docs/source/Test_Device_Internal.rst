Internal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:DEVice:INTernal

.. code-block:: python

	TEST:DEVice:INTernal



.. autoclass:: RsAreg.Implementations.Test.Device.Internal.InternalCls
	:members:
	:undoc-members:
	:noindex: