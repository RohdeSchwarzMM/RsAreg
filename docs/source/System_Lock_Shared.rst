Shared
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:LOCK:SHARed:STRing

.. code-block:: python

	SYSTem:LOCK:SHARed:STRing



.. autoclass:: RsAreg.Implementations.System.Lock.Shared.SharedCls
	:members:
	:undoc-members:
	:noindex: