Keyboard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FPANel:KEYBoard:LAYout

.. code-block:: python

	FPANel:KEYBoard:LAYout



.. autoclass:: RsAreg.Implementations.Fpanel.Keyboard.KeyboardCls
	:members:
	:undoc-members:
	:noindex: