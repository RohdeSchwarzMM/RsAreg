Attenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:ATTenuation

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:ATTenuation



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.Attenuation.AttenuationCls
	:members:
	:undoc-members:
	:noindex: