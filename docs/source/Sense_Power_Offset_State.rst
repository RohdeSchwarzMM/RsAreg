State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:OFFSet:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:OFFSet:STATe



.. autoclass:: RsAreg.Implementations.Sense.Power.Offset.State.StateCls
	:members:
	:undoc-members:
	:noindex: