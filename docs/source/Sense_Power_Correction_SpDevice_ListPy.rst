ListPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:CORRection:SPDevice:LIST

.. code-block:: python

	SENSe<CH>:[POWer]:CORRection:SPDevice:LIST



.. autoclass:: RsAreg.Implementations.Sense.Power.Correction.SpDevice.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex: