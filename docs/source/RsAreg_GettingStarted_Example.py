"""Getting started - how to work with RsAreg Python package.
This example performs basic RF settings on an R&S AREG instrument.
It shows the RsAreg calls and their corresponding SCPI commands.
Notice that the python RsAreg interfaces track the SCPI commands syntax."""

from RsAreg import *

# Open the session
areg = RsAreg('TCPIP::10.102.52.44::HISLIP', False, False)
# Greetings, stranger...
print(f'Hello, I am: {areg.utilities.idn_string}')

#   SOURce:FREQuency:FIXed 223000000
areg.source.frequency.cw.set_value(223E6)

areg.source.areGenerator.radar.base.set_attenuation(10)

# Close the session
areg.close()
