Usensor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SLISt:SCAN:USENsor

.. code-block:: python

	SLISt:SCAN:USENsor



.. autoclass:: RsAreg.Implementations.Slist.Scan.Usensor.UsensorCls
	:members:
	:undoc-members:
	:noindex: