Device
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: HCOPy:DEVice:LANGuage

.. code-block:: python

	HCOPy:DEVice:LANGuage



.. autoclass:: RsAreg.Implementations.HardCopy.Device.DeviceCls
	:members:
	:undoc-members:
	:noindex: