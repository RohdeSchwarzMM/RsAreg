Calibration
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration<HW>:CONTinueonerror
	single: CALibration<HW>:DEBug

.. code-block:: python

	CALibration<HW>:CONTinueonerror
	CALibration<HW>:DEBug



.. autoclass:: RsAreg.Implementations.Calibration.CalibrationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.calibration.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Calibration_All.rst
	Calibration_Data.rst
	Calibration_Delay.rst
	Calibration_Frequency.rst
	Calibration_Level.rst
	Calibration_Roscillator.rst
	Calibration_Selected.rst
	Calibration_Tselected.rst