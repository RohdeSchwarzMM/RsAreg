Antenna
----------------------------------------





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.radar.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Radar_Antenna_Custom.rst
	Source_AreGenerator_Radar_Antenna_Reg.rst