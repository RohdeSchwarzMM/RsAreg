Eirp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:RADar:EIRP:SENSor
	single: [SOURce<HW>]:AREGenerator:RADar:EIRP

.. code-block:: python

	[SOURce<HW>]:AREGenerator:RADar:EIRP:SENSor
	[SOURce<HW>]:AREGenerator:RADar:EIRP



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.Eirp.EirpCls
	:members:
	:undoc-members:
	:noindex: