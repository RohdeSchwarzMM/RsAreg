Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:[LEVel]:[IMMediate]:RCL

.. code-block:: python

	[SOURce<HW>]:POWer:[LEVel]:[IMMediate]:RCL



.. autoclass:: RsAreg.Implementations.Source.Power.Level.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: