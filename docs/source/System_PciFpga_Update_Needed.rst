Needed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PCIFpga:UPDate:NEEDed:[STATe]

.. code-block:: python

	SYSTem:PCIFpga:UPDate:NEEDed:[STATe]



.. autoclass:: RsAreg.Implementations.System.PciFpga.Update.Needed.NeededCls
	:members:
	:undoc-members:
	:noindex: