Ethernet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:SCPI:ETHernet:[ACTive]

.. code-block:: python

	SYSTem:COMMunicate:SCPI:ETHernet:[ACTive]



.. autoclass:: RsAreg.Implementations.System.Communicate.Scpi.Ethernet.EthernetCls
	:members:
	:undoc-members:
	:noindex: