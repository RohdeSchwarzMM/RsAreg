Synchronize
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SRTime:SYNChronize

.. code-block:: python

	SYSTem:SRTime:SYNChronize



.. autoclass:: RsAreg.Implementations.System.Srtime.Synchronize.SynchronizeCls
	:members:
	:undoc-members:
	:noindex: