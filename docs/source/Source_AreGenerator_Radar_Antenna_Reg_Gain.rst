Gain
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:RADar:ANTenna:REG:GAIN:RX
	single: [SOURce<HW>]:AREGenerator:RADar:ANTenna:REG:GAIN:TX

.. code-block:: python

	[SOURce<HW>]:AREGenerator:RADar:ANTenna:REG:GAIN:RX
	[SOURce<HW>]:AREGenerator:RADar:ANTenna:REG:GAIN:TX



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.Antenna.Reg.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: