InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:CHANnel:INPut:NOMGain
	single: [SOURce<HW>]:AREGenerator:CHANnel:INPut:RELLevel

.. code-block:: python

	[SOURce<HW>]:AREGenerator:CHANnel:INPut:NOMGain
	[SOURce<HW>]:AREGenerator:CHANnel:INPut:RELLevel



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Channel.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: