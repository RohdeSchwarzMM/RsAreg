Secondary<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.areGenerator.osetup.multiInstrument.secondary.repcap_index_get()
	driver.source.areGenerator.osetup.multiInstrument.secondary.repcap_index_set(repcap.Index.Nr1)





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.SecondaryCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.osetup.multiInstrument.secondary.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Add.rst
	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Execute.rst
	Source_AreGenerator_Osetup_MultiInstrument_Secondary_Remove.rst