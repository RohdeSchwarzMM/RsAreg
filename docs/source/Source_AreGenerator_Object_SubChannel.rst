SubChannel<Subchannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.source.areGenerator.object.subChannel.repcap_subchannel_get()
	driver.source.areGenerator.object.subChannel.repcap_subchannel_set(repcap.Subchannel.Nr1)





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.SubChannel.SubChannelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.object.subChannel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Object_SubChannel_Doppler.rst
	Source_AreGenerator_Object_SubChannel_State.rst