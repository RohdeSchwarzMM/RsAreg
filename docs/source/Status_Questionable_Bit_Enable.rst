Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<BITNR>:ENABle

.. code-block:: python

	STATus:QUEStionable:BIT<BITNR>:ENABle



.. autoclass:: RsAreg.Implementations.Status.Questionable.Bit.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: