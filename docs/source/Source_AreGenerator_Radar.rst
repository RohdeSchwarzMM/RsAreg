Radar
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:RADar:LSENsitivity

.. code-block:: python

	[SOURce<HW>]:AREGenerator:RADar:LSENsitivity



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.RadarCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.radar.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Radar_Antenna.rst
	Source_AreGenerator_Radar_Base.rst
	Source_AreGenerator_Radar_Dbypass.rst
	Source_AreGenerator_Radar_Eirp.rst
	Source_AreGenerator_Radar_Ota.rst
	Source_AreGenerator_Radar_Power.rst