Restore
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DEVice:SETTings:RESTore

.. code-block:: python

	DEVice:SETTings:RESTore



.. autoclass:: RsAreg.Implementations.Device.Settings.Restore.RestoreCls
	:members:
	:undoc-members:
	:noindex: