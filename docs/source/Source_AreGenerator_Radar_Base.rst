Base
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:RADar:BASE:ATTenuation

.. code-block:: python

	[SOURce<HW>]:AREGenerator:RADar:BASE:ATTenuation



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.Base.BaseCls
	:members:
	:undoc-members:
	:noindex: