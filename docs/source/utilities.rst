RsAreg Utilities
==========================

.. _Utilities:

.. autoclass:: RsAreg.CustomFiles.utilities.Utilities()
   :members:
   :undoc-members:
   :special-members: enable_properties
   :noindex:
   :member-order: bysource
