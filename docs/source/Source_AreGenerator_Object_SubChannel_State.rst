State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:[STATe]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:[STATe]



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.SubChannel.State.StateCls
	:members:
	:undoc-members:
	:noindex: