Usb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:USB:RESource

.. code-block:: python

	SYSTem:COMMunicate:USB:RESource



.. autoclass:: RsAreg.Implementations.System.Communicate.Usb.UsbCls
	:members:
	:undoc-members:
	:noindex: