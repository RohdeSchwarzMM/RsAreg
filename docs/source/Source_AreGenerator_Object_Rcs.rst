Rcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:RCS

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:RCS



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.Rcs.RcsCls
	:members:
	:undoc-members:
	:noindex: