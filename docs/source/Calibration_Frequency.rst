Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:FREQuency:SWPoints

.. code-block:: python

	CALibration:FREQuency:SWPoints



.. autoclass:: RsAreg.Implementations.Calibration.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: