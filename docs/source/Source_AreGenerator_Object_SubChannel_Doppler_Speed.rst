Speed
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:DOPPler:[SPEed]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OBJect<CH>:[SUBChannel<ST>]:DOPPler:[SPEed]



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.SubChannel.Doppler.Speed.SpeedCls
	:members:
	:undoc-members:
	:noindex: