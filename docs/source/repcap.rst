RepCaps
=========

HwInstance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_hwInstance_set(repcap.HwInstance.InstA)
	# Range:
	InstA .. InstH
	# All values (8x):
	InstA | InstB | InstC | InstD | InstE | InstF | InstG | InstH

BitNumberNull
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BitNumberNull.Nr0
	# Range:
	Nr0 .. Nr15
	# All values (16x):
	Nr0 | Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7
	Nr8 | Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15

Channel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Channel.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

Index
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Index.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

Level
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Level.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

ObjectIx
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ObjectIx.Nr1
	# Range:
	Nr1 .. Nr12
	# All values (12x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12

Subchannel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Subchannel.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

