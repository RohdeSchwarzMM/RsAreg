Measure
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:SELected:[MEASure]

.. code-block:: python

	CALibration:SELected:[MEASure]



.. autoclass:: RsAreg.Implementations.Calibration.Selected.Measure.MeasureCls
	:members:
	:undoc-members:
	:noindex: