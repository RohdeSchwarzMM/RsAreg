Custom
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:RADar:ANTenna:CUSTom:[STATe]

.. code-block:: python

	[SOURce<HW>]:AREGenerator:RADar:ANTenna:CUSTom:[STATe]



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.Antenna.Custom.CustomCls
	:members:
	:undoc-members:
	:noindex: