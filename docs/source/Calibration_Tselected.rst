Tselected
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CALibration:TSELected:CATalog
	single: CALibration:TSELected:STEP
	single: CALibration:TSELected:[MEASure]

.. code-block:: python

	CALibration:TSELected:CATalog
	CALibration:TSELected:STEP
	CALibration:TSELected:[MEASure]



.. autoclass:: RsAreg.Implementations.Calibration.Tselected.TselectedCls
	:members:
	:undoc-members:
	:noindex: