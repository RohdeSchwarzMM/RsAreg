Channel
----------------------------------------





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.channel.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Channel_InputPy.rst
	Source_AreGenerator_Channel_Output.rst