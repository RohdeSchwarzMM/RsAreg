Lockout
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST<HW>:REMote:LOCKout:[STATe]

.. code-block:: python

	TEST<HW>:REMote:LOCKout:[STATe]



.. autoclass:: RsAreg.Implementations.Test.Remote.Lockout.LockoutCls
	:members:
	:undoc-members:
	:noindex: