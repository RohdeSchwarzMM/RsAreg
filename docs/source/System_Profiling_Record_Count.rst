Count
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:PROFiling:RECord:COUNt:MAX
	single: SYSTem:PROFiling:RECord:COUNt

.. code-block:: python

	SYSTem:PROFiling:RECord:COUNt:MAX
	SYSTem:PROFiling:RECord:COUNt



.. autoclass:: RsAreg.Implementations.System.Profiling.Record.Count.CountCls
	:members:
	:undoc-members:
	:noindex: