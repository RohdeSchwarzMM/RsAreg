Ota
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:RADar:OTA:OFFSet

.. code-block:: python

	[SOURce<HW>]:AREGenerator:RADar:OTA:OFFSet



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Radar.Ota.OtaCls
	:members:
	:undoc-members:
	:noindex: