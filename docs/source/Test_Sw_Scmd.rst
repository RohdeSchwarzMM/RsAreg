Scmd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST<HW>:SW:SCMD

.. code-block:: python

	TEST<HW>:SW:SCMD



.. autoclass:: RsAreg.Implementations.Test.Sw.Scmd.ScmdCls
	:members:
	:undoc-members:
	:noindex: