RfOff
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:ROSCillator:EXTernal:RFOFf:[STATe]

.. code-block:: python

	[SOURce]:ROSCillator:EXTernal:RFOFf:[STATe]



.. autoclass:: RsAreg.Implementations.Source.Roscillator.External.RfOff.RfOffCls
	:members:
	:undoc-members:
	:noindex: