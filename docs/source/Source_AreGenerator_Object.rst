Object<ObjectIx>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.source.areGenerator.object.repcap_objectIx_get()
	driver.source.areGenerator.object.repcap_objectIx_set(repcap.ObjectIx.Nr1)





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Object.ObjectCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.object.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Object_All.rst
	Source_AreGenerator_Object_Attenuation.rst
	Source_AreGenerator_Object_Range.rst
	Source_AreGenerator_Object_Rcs.rst
	Source_AreGenerator_Object_SubChannel.rst