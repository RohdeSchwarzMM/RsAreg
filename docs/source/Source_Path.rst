Path
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:PATH:COUNt

.. code-block:: python

	[SOURce]:PATH:COUNt



.. autoclass:: RsAreg.Implementations.Source.Path.PathCls
	:members:
	:undoc-members:
	:noindex: