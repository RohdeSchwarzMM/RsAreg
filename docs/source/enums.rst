Enums
=========

AregDopplerUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregDopplerUnit.FREQuency
	# All values (2x):
	FREQuency | SPEed

AregPowSens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregPowSens.SEN1
	# All values (5x):
	SEN1 | SEN2 | SEN3 | SEN4 | UDEFined

AregRadarPowIndicator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AregRadarPowIndicator.BAD
	# All values (4x):
	BAD | GOOD | OFF | WEAK

ByteOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ByteOrder.NORMal
	# All values (2x):
	NORMal | SWAPped

CalDataMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataMode.CUSTomer
	# All values (2x):
	CUSTomer | FACTory

CalDataUpdate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataUpdate.BBFRC
	# All values (6x):
	BBFRC | FREQuency | IALL | LEVel | LEVForced | RFFRC

Colour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Colour.GREen
	# All values (4x):
	GREen | NONE | RED | YELLow

DevExpFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DevExpFormat.CGPRedefined
	# All values (4x):
	CGPRedefined | CGUSer | SCPI | XML

DispKeybLockMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DispKeybLockMode.DISabled
	# All values (5x):
	DISabled | DONLy | ENABled | TOFF | VNConly

ErFpowSensMapping
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ErFpowSensMapping.SENS1
	# Last value:
	value = enums.ErFpowSensMapping.UNMapped
	# All values (9x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4
	UNMapped

ErFpowSensSourceAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ErFpowSensSourceAreg.USER
	# All values (1x):
	USER

FormData
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormData.ASCii
	# All values (2x):
	ASCii | PACKed

FormStatReg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormStatReg.ASCii
	# All values (4x):
	ASCii | BINary | HEXadecimal | OCTal

FrontPanelLayout
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrontPanelLayout.DIGits
	# All values (2x):
	DIGits | LETTers

HardCopyImageFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyImageFormat.BMP
	# All values (4x):
	BMP | JPG | PNG | XPM

HardCopyRegion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyRegion.ALL
	# All values (2x):
	ALL | DIALog

IecDevId
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecDevId.AUTO
	# All values (2x):
	AUTO | USER

IecTermMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecTermMode.EOI
	# All values (2x):
	EOI | STANdard

InclExcl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InclExcl.EXCLude
	# All values (2x):
	EXCLude | INCLude

KbLayout
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.KbLayout.CHINese
	# Last value:
	value = enums.KbLayout.SWEDish
	# All values (20x):
	CHINese | DANish | DUTBe | DUTCh | ENGLish | ENGUK | ENGUS | FINNish
	FREBe | FRECa | FRENch | GERMan | ITALian | JAPanese | KORean | NORWegian
	PORTuguese | RUSSian | SPANish | SWEDish

NetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetMode.AUTO
	# All values (2x):
	AUTO | STATic

Parity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Parity.EVEN
	# All values (3x):
	EVEN | NONE | ODD

PixelTestPredefined
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PixelTestPredefined.AUTO
	# Last value:
	value = enums.PixelTestPredefined.WHITe
	# All values (9x):
	AUTO | BLACk | BLUE | GR25 | GR50 | GR75 | GREen | RED
	WHITe

PowSensDisplayPriority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensDisplayPriority.AVERage
	# All values (2x):
	AVERage | PEAK

PowSensFiltType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensFiltType.AUTO
	# All values (3x):
	AUTO | NSRatio | USER

RecScpiCmdMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RecScpiCmdMode.AUTO
	# All values (4x):
	AUTO | DAUTo | MANual | OFF

RoscBandWidtExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscBandWidtExt.NARRow
	# All values (2x):
	NARRow | WIDE

RoscFreqExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscFreqExt._10MHZ
	# All values (3x):
	_10MHZ | _13MHZ | _5MHZ

RoscSourSetup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscSourSetup.ELOop
	# All values (3x):
	ELOop | EXTernal | INTernal

Rs232BdRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232BdRate._115200
	# All values (7x):
	_115200 | _19200 | _2400 | _38400 | _4800 | _57600 | _9600

Rs232StopBits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232StopBits._1
	# All values (2x):
	_1 | _2

SelftLev
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLev.CUSTomer
	# All values (3x):
	CUSTomer | PRODuction | SERVice

SelftLevWrite
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLevWrite.CUSTomer
	# All values (4x):
	CUSTomer | NONE | PRODuction | SERVice

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

StateExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StateExtended.DEFault
	# All values (3x):
	DEFault | OFF | ON

Test
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Test._0
	# All values (4x):
	_0 | _1 | RUNning | STOPped

TestCalSelected
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestCalSelected._0
	# All values (2x):
	_0 | _1

TimeProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeProtocol._0
	# All values (6x):
	_0 | _1 | NONE | NTP | OFF | ON

UnitAngleAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitAngleAreg.DEGree
	# All values (2x):
	DEGree | RADian

UnitLengthAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitLengthAreg.CM
	# All values (3x):
	CM | FT | M

UnitPowSens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPowSens.DBM
	# All values (3x):
	DBM | DBUV | WATT

UnitRcsAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitRcsAreg.DBSM
	# All values (2x):
	DBSM | SM

UnitShiftAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitShiftAreg.HZ
	# All values (3x):
	HZ | KHZ | MHZ

UnitSpeedAreg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitSpeedAreg.KMH
	# All values (3x):
	KMH | MPH | MPS

UpdPolicyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpdPolicyMode.CONFirm
	# All values (3x):
	CONFirm | IGNore | STRict

