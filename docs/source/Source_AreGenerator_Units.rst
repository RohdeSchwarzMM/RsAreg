Units
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AREGenerator:UNITs:ANGLe
	single: [SOURce<HW>]:AREGenerator:UNITs:DOPPler
	single: [SOURce<HW>]:AREGenerator:UNITs:RANGe
	single: [SOURce<HW>]:AREGenerator:UNITs:RCS
	single: [SOURce<HW>]:AREGenerator:UNITs:SHIFt
	single: [SOURce<HW>]:AREGenerator:UNITs:SPEed

.. code-block:: python

	[SOURce<HW>]:AREGenerator:UNITs:ANGLe
	[SOURce<HW>]:AREGenerator:UNITs:DOPPler
	[SOURce<HW>]:AREGenerator:UNITs:RANGe
	[SOURce<HW>]:AREGenerator:UNITs:RCS
	[SOURce<HW>]:AREGenerator:UNITs:SHIFt
	[SOURce<HW>]:AREGenerator:UNITs:SPEed



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Units.UnitsCls
	:members:
	:undoc-members:
	:noindex: