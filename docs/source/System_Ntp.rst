Ntp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:NTP:HOSTname

.. code-block:: python

	SYSTem:NTP:HOSTname



.. autoclass:: RsAreg.Implementations.System.Ntp.NtpCls
	:members:
	:undoc-members:
	:noindex: