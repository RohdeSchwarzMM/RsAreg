Remove
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary<ST>:REMove

.. code-block:: python

	[SOURce<HW>]:AREGenerator:OSETup:MULTiinstrument:SECondary<ST>:REMove



.. autoclass:: RsAreg.Implementations.Source.AreGenerator.Osetup.MultiInstrument.Secondary.Remove.RemoveCls
	:members:
	:undoc-members:
	:noindex: