State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:VNC:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:VNC:[STATe]



.. autoclass:: RsAreg.Implementations.System.Security.Network.Vnc.State.StateCls
	:members:
	:undoc-members:
	:noindex: