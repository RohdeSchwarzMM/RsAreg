Select
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:CORRection:SPDevice:SELect

.. code-block:: python

	SENSe<CH>:[POWer]:CORRection:SPDevice:SELect



.. autoclass:: RsAreg.Implementations.Sense.Power.Correction.SpDevice.Select.SelectCls
	:members:
	:undoc-members:
	:noindex: