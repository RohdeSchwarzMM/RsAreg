RsAreg API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsAreg('TCPIP::192.168.2.101::hislip0')
	# HwInstance range: InstA .. InstH
	rc = driver.repcap_hwInstance_get()
	driver.repcap_hwInstance_set(repcap.HwInstance.InstA)

.. autoclass:: RsAreg.RsAreg
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Calibration.rst
	Device.rst
	Diagnostic.rst
	Display.rst
	FormatPy.rst
	Fpanel.rst
	HardCopy.rst
	Initiate.rst
	Kboard.rst
	MassMemory.rst
	Memory.rst
	Read.rst
	Sense.rst
	Slist.rst
	Source.rst
	Status.rst
	System.rst
	Test.rst