BgInfo
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: DIAGnostic<HW>:BGINfo
	single: DIAGnostic<HW>:BGINfo:CATalog

.. code-block:: python

	DIAGnostic<HW>:BGINfo
	DIAGnostic<HW>:BGINfo:CATalog



.. autoclass:: RsAreg.Implementations.Diagnostic.BgInfo.BgInfoCls
	:members:
	:undoc-members:
	:noindex: