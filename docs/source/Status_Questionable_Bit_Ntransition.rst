Ntransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<BITNR>:NTRansition

.. code-block:: python

	STATus:QUEStionable:BIT<BITNR>:NTRansition



.. autoclass:: RsAreg.Implementations.Status.Questionable.Bit.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: