Ntransition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:OPERation:BIT<BITNR>:NTRansition

.. code-block:: python

	STATus:OPERation:BIT<BITNR>:NTRansition



.. autoclass:: RsAreg.Implementations.Status.Operation.Bit.Ntransition.NtransitionCls
	:members:
	:undoc-members:
	:noindex: