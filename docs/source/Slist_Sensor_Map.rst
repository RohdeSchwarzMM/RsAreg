Map
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SLISt:SENSor:MAP

.. code-block:: python

	SLISt:SENSor:MAP



.. autoclass:: RsAreg.Implementations.Slist.Sensor.Map.MapCls
	:members:
	:undoc-members:
	:noindex: