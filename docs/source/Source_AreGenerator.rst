AreGenerator
----------------------------------------





.. autoclass:: RsAreg.Implementations.Source.AreGenerator.AreGeneratorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.areGenerator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_AreGenerator_Channel.rst
	Source_AreGenerator_Object.rst
	Source_AreGenerator_Osetup.rst
	Source_AreGenerator_Radar.rst
	Source_AreGenerator_Units.rst