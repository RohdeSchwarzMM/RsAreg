from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from ....Internal.Types import DataType
from ....Internal.ArgSingleList import ArgSingleList
from ....Internal.ArgSingle import ArgSingle


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class LengthCls:
	"""Length commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("length", core, parent)

	def get(self, path: str = None) -> int:
		"""SCPI: MMEMory:CATalog:LENGth \n
		Snippet: value: int = driver.massMemory.catalog.length.get(path = 'abc') \n
		Returns the number of files in the current or in the specified directory. \n
			:param path: string String parameter to specify the directory. If the directory is omitted, the command queries the content of the current directory, queried with method RsAreg.MassMemory.currentDirectory command.
			:return: file_count: integer Number of files."""
		param = ArgSingleList().compose_cmd_string(ArgSingle('path', path, DataType.String, None, is_optional=True))
		response = self._core.io.query_str(f'MMEMory:CATalog:LENGth? {param}'.rstrip())
		return Conversions.str_to_int(response)
